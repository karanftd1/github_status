FROM python:3.7.2

# Create the working directory (and set it as the working directory)
RUN mkdir -p /git
WORKDIR /git

# Install the package dependencies (this step is separated
# from copying all the source code to avoid having to
# re-install all python packages defined in requirements.txt
# whenever any source code change is made)
COPY ./requirements.txt /git/requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Copy the source code into the container
COPY . /git

RUN mkdir -p reports

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]
