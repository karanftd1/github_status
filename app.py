import os, requests, re, time, csv
import logging
from gitstats import GitStats
from sys import stdin, exit

# create logger 
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

# reading environment variable for authorization token (if set)
authtoken = ('authorization' in os.environ and os.environ['authorization']) or None

'''
    method that iterates on gitrepos and call github api to fetch relevant info
'''
def get_github_stats():
    repos = read_input()
    gitstats = []
    is_rate_limit_exceeded = False
    # setting up base url for github api
    github_api_base_url = 'https://api.github.com/repos/%s/%s/commits/master'
    for repo in repos:
        repo_parts = repo.split('/')
        url = github_api_base_url % (repo_parts[0], repo_parts[1])
        response = None
        try:
            # if authtoken is not None then set authorization headers
            if authtoken is not None:
                response = requests.get(url, headers = {'Authorization':'token ' + authtoken})
            else:
                # raise error if it is related to http
                response = requests.get(url)
            
            response.raise_for_status()
            stats = response.json()
            g = GitStats(repo, url, stats['commit']['author']['date'], stats['commit']['author']['name'])
            json_repr = g.toJSON()
            gitstats.append(json_repr)

        # check of rate limit exceeded
        except (requests.exceptions.HTTPError, requests.exceptions.RequestException) as err:

            # check for response status and response info
            if response is not None and response.status_code == 403:
                rate_limit = response.headers['X-RateLimit-Remaining'] or None

                # check error due to rate limit exceeded
                if rate_limit is not None:
                    # set rate limit exceeded true and break
                    reset_epoch_time = response.headers['X-RateLimit-Reset']
                    reset_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(reset_epoch_time)))
                    is_rate_limit_exceeded = True
                    logger.warn("Rate limit exceeded, please try again on or after: " + reset_time)
                    break
                    
            # log the error message
            logger.error(err)
            exit(1)

    # write into CSV file
    write_csv(is_rate_limit_exceeded, gitstats)

''' 
    Method to read input from STDIN.
'''
def read_input():
    repos = []
    print("Please enter orgname/repo in each line in format orgname/repo.")
    while True:
        userinput = stdin.readline().strip("\n")
        logger.debug(userinput)
        if userinput == "":
            logger.info("Got empty input, will start fetching response")
            break
        else:
            # validating userinput to check format of repo
            match = re.match("([-_\\w]+)\\/([-_.\\w]+)", userinput)
            # if regex matches and there is exactly two groups (one for orgname and one for repo)
            if match is not None and len(match.groups()) == 2:
                repos.append(userinput)
            else:
                logger.error("Invalid input line " + userinput)
    return repos


'''
    Write to CSV file   
'''
def write_csv(is_rate_limit_exceeded, githubstats):
    # create unique file name
    timestr = time.strftime("%Y%m%d-%H%M%S")
    csv_file_name = 'csv_report_' + timestr + ".csv"

    # validate the output
    if len(githubstats) != 0:
        # check if rate limit exceeded
        if is_rate_limit_exceeded:
            logger.info("Rate limited exceeded, however writing the proceesed results in file: " + csv_file_name)
        else:
            logger.info("Writing the processed results in file: " + csv_file_name)

        # write into file
        try:
            with open('reports/' + csv_file_name, 'w') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=githubstats[0].keys())
                writer.writeheader()
                for data in githubstats:
                    writer.writerow(data)
                logger.info(os.path.abspath(csvfile.name))
            with open('reports/' + csv_file_name) as f:
                content = f.readlines()
            # you may also want to remove whitespace characters like `\n` at the end of each line
            content = [x.strip() for x in content] 
            logger.info(content)
        except IOError as errno:
                logger.error("I/O error: {0}".format(errno))

if __name__== "__main__":
  get_github_stats()    
