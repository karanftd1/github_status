# github_status

# Task
* Write an application/script that accepts a list of public Github repositories and prints out
the name, clone URL, date of latest commit and name of latest author for each one

# Input
* Read plain text list of repositories from stdin
* One repo per input line, format: $orgname/$repo, e.g. kubernetes/charts
* Other parameters/env vars as needed, should be documented

# Output
* One line per input repo in CSV or TSV format plus one header line to stdout

# Notes
* Solutions can be done in Python, Golang, NodeJS or Bash
* Solution should include dependency management for the language chosen
* Please provide a Dockerfile

#### PROBLEM STATEMENT
Develop an application/script that accepts a list of public github repositories and printout url,latest author name,name and last commit date

### SOLUTION DETAILS

* Implemented python script to take list of repos from STDIN.
* Once read, it iterates on repos line by line and fetch information using GitHub API
* And, it writes to CSV

### NOTE:
* As GitHub API has throttling in place, you can only make 60 API calls per hour (as unauthorized user)
* If token is available, we can make more number of calls which is around 5000 API calls per hour.

### Running Instructions

```sh
$ git clone https://gitlab.com/karanftd1/github_status
$ cd github_status
$ virtualenv -p python3 <virtual-env>
$ source <virtual-env>/bin/activate
$ authorization=<your-github-user-token> python app.py
```

### Using Docker
The application root directory has dockerfile which can be used to build image to prepare and install dependencies and build docker image. Use command to build docker image from directory where Dockekrfile is:

```sh
docker build -t <image-name> .
```

Once build, use command to run application:
```sh
docker run -v ~/reports:/reports -i <image-name>
```

Run with user-github-token
```sh
docker run -e authorization=<your_personal_access_token> -v ~/reports:/reports -i <image-name>
```
